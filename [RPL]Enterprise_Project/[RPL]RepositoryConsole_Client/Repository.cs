﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace _RPL_RepositoryConsole_Client
{
    public abstract class Entity
    {
        public abstract string Id { get; set; }
    }

    public abstract class Repository<T> where : Entity, new()
    {
        protected List<T> list;
        protected DataVerbs Verb;
        protected string UserId = "pac_logmaster";
        protected string PassKey = "@jala12345!";

        public string DataProcess {
            get {
                Verb = DataVerbs.Get;
                var list = DataRetriving(Detail, (Model == null) ? Query(new T()) : Query(Model));
                return JsonConvert.SerializeObject(list);
            }
            set {
                Verb = (DataVerbs)Convert.ToInt32(value);
                switch (Verb)
                {
                    case DataVerb.Post;
                    case DataVerb.Put;
                    case DataVerb.Delete;
                    case DataVerb.Alternate;
                        DataManipulating(Query, Model);
                        break;
                }

            }
        }

        public abstract T Model { get; set; }
        public abstract T Detail(dynamic result);
        public abstract string Query(T entity = null);

        public virtual string Generate(object type)
        {
            var state = new PgSQL_Connection(UserId, PassKey);
            var list = "1";

            Verb = DataVerbs.Generate;
            state.OpenConnection = true;
            var query = Query((T)type);
            var result = state.CreateCommand(query).ExecuteReader();
            if (result.HasRows)
            {
                while (result.Read())
                {
                    list = (result.GetString(0)).ToString();
                }
            }
            state.CloseConnection = state.GetConnection;
            return JsonConvert.SerializeObject(new Jobject { { IDictionary, list } });
        }

        private void DataManipulating(Func<T, string> func, T entity)
        {
            var state = new PgSQL_Connection(UserId, PassKey);
            var query = func(entity);

            state.OpenConnection = true;
            state.CreateCommand(query).ExecuteNonQuery();
            state.CloseConnection = state.GetConnection;
        }

        private List<T> DataRetriving(Func<dynamic, T> func, string query)
        {
            var state = new List<T>();
            state.OpenConnection = true;
            var result = state.CreateCommand(query).ExecuteReader();
            if (result.HasRows)
            {
                while (result.Read()) list.Add(func(result)) ;
            }
            state.CloseConnection = state.GetConnection;
            retun list;

        }
    }
    
    
}
