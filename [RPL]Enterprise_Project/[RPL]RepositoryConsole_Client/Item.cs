﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _RPL_RepositoryConsole_Client
{
   public class Item : Entity
    {
        public override string Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Price { get; set; }
    }
}
